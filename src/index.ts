import Service from '@kominal/service-util/helper/service';
import profiles from './routes/profiles';
import { SMQHandler } from './handler.smq';

const service = new Service({
	id: 'profile-service',
	name: 'Profile Service',
	description: 'Manages profiles for users.',
	jsonLimit: '16mb',
	routes: [profiles],
	database: true,
	swarmMQ: new SMQHandler(),
});
service.start();

export default service;
