import ProfileDatabase from '@kominal/connect-models/profile/profile.database';
import { ProfileEncrypted } from '@kominal/connect-models/profile/profile.encrypted';
import Router from '@kominal/service-util/helper/router';
import { verifyParameter } from '@kominal/service-util/helper/util';
import axios from 'axios';
import { STACK_NAME, SERVICE_TOKEN, TIMEOUT } from '@kominal/service-util/helper/environment';
import service from '..';

const router = new Router();

/**
 * Get all active profiles
 * @group Protected
 * @security JWT
 * @route GET /
 * @consumes application/json
 * @produces application/json
 * @returns {Array} 200 - Array of ProfileEncrypted
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.getAsUser('/', async (req, res, userId) => {
	const profiles: ProfileEncrypted[] = [];
	for (const profile of await ProfileDatabase.find({ userId, active: true })) {
		profiles.push({
			id: profile._id,
			userId: profile.get('userId'),
			active: profile.get('active'),
			displayname: {
				data: profile.get('displayname'),
				iv: profile.get('displaynameIv'),
			},
			publicKey: {
				data: profile.get('publicKey'),
				iv: profile.get('publicKeyIv'),
			},
			displaynameHash: profile.get('displaynameHash'),
			keyDisplayname: {
				data: profile.get('keyDisplayname'),
				iv: profile.get('keyDisplaynameIv'),
			},
			keyMasterEncryptionKey: {
				data: profile.get('keyMasterEncryptionKey'),
				iv: profile.get('keyMasterEncryptionKeyIv'),
			},
			avatarStorageId: profile.get('avatarStorageId'),
			lastUpdated: profile.get('lastUpdated'),
		});
	}

	res.status(200).send(profiles);
});

/**
 * Get all active profiles
 * @group Protected
 * @security JWT
 * @route GET /displayname/{displaynameHash}
 * @consumes application/json
 * @produces application/json
 * @returns {Array} 200 - Array of ProfileEncrypted
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.getAsGuest('/displayname/:displaynameHash', async (req, res) => {
	const { displaynameHash } = req.params;
	verifyParameter(displaynameHash);

	const profile = await ProfileDatabase.findOne({ displaynameHash, active: true });

	if (!profile) {
		throw 'error.profile.notfound';
	}

	const profileEncrypted: ProfileEncrypted = {
		id: profile._id,
		userId: profile.get('userId'),
		active: profile.get('active'),
		displayname: {
			data: profile.get('displayname'),
			iv: profile.get('displaynameIv'),
		},
		publicKey: {
			data: profile.get('publicKey'),
			iv: profile.get('publicKeyIv'),
		},
		displaynameHash: profile.get('displaynameHash'),
		keyDisplayname: {
			data: profile.get('keyDisplayname'),
			iv: profile.get('keyDisplaynameIv'),
		},
		keyMasterEncryptionKey: undefined,
		avatarStorageId: profile.get('avatarStorageId'),
		lastUpdated: profile.get('lastUpdated'),
	};

	res.status(200).send(profileEncrypted);
});

/**
 * Get all active profiles
 * @group Protected
 * @security JWT
 * @route GET /{profileId}/{lastUpdated}
 * @consumes application/json
 * @produces application/json
 * @returns {Array} 200 - Array of ProfileEncrypted
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.getAsUser('/:profileId/:lastUpdated*?', async (req, res) => {
	const { profileId, lastUpdated } = req.params;
	verifyParameter(profileId);

	const profile = await ProfileDatabase.findOne({ _id: profileId });

	if (!profile) {
		throw 'error.profile.notfound';
	}

	if (lastUpdated && profile.get('lastUpdated') && profile.get('lastUpdated').getTime() === Number(lastUpdated)) {
		res.status(304).send();
		return;
	}

	const profileEncrypted: ProfileEncrypted = {
		id: profile._id,
		userId: profile.get('userId'),
		active: profile.get('active'),
		displayname: {
			data: profile.get('displayname'),
			iv: profile.get('displaynameIv'),
		},
		publicKey: {
			data: profile.get('publicKey'),
			iv: profile.get('publicKeyIv'),
		},
		displaynameHash: profile.get('displaynameHash'),
		keyDisplayname: {
			data: profile.get('keyDisplayname'),
			iv: profile.get('keyDisplaynameIv'),
		},
		keyMasterEncryptionKey: undefined,
		avatarStorageId: profile.get('avatarStorageId'),
		lastUpdated: profile.get('lastUpdated'),
	};

	res.status(200).send(profileEncrypted);
});

/**
 * Create a profile
 * @group Protected
 * @security JWT
 * @route POST /
 * @consumes application/json
 * @produces application/json
 * @param {string} displaynameHash.body.required - The hash of the displayname
 * @param {AESEncrypted} displayname.body.required - The displayname encrypted with the masterEncryptionKey
 * @returns {Array} 200 - Array of ProfileEncrypted
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.postAsUser('/', async (req, res, userId) => {
	const { displaynameHash, displayname, publicKey, keyDisplayname, keyMasterEncryptionKey } = req.body;
	verifyParameter(
		displaynameHash,
		displayname?.data,
		displayname?.iv,
		publicKey?.data,
		publicKey?.iv,
		keyDisplayname?.data,
		keyDisplayname?.iv,
		keyMasterEncryptionKey?.data,
		keyMasterEncryptionKey?.iv
	);

	if (await ProfileDatabase.findOne({ displaynameHash, active: true })) {
		throw 'error.displayname.inuse';
	}

	if ((await ProfileDatabase.countDocuments({ userId, active: true })) >= 3) {
		throw 'error.profiles.tomany';
	}

	const profile = await ProfileDatabase.create({
		userId,
		active: true,
		displayname: displayname.data,
		displaynameIv: displayname.iv,
		publicKey: publicKey.data,
		publicKeyIv: publicKey.iv,
		displaynameHash,
		keyDisplayname: keyDisplayname.data,
		keyDisplaynameIv: keyDisplayname.iv,
		keyMasterEncryptionKey: keyMasterEncryptionKey.data,
		keyMasterEncryptionKeyIv: keyMasterEncryptionKey.iv,
		lastUpdated: new Date(),
	});

	service.getSMQClient()?.publish('TOPIC', `DIRECT.USER.${userId}`, 'PROFILE.CREATED', { id: profile._id });

	res.status(200).send();
});

/**
 * Update a profile
 * @group Protected
 * @security JWT
 * @route PUT /{profileId}
 * @consumes application/json
 * @produces application/json
 * @param {string} displaynameHash.body.required - The hash of the displayname
 * @param {AESEncrypted} displayname.body.required - The displayname encrypted with the masterEncryptionKey
 * @returns {Array} 200 - Array of ProfileEncrypted
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.putAsUser('/:profileId', async (req, res, userId) => {
	const { profileId } = req.params;
	const { avatarStorageId, displayname } = req.body;

	if (!(await ProfileDatabase.findOne({ userId, _id: profileId, active: true }))) {
		throw 'error.profile.notfound';
	}

	let update: any = {};

	if (displayname) {
		const { displaynameHash, displayname, keyDisplayname } = req.body;
		verifyParameter(displaynameHash, displayname?.data, displayname?.iv, keyDisplayname?.data, keyDisplayname?.iv);

		update = {
			displayname: displayname.data,
			displaynameIv: displayname.iv,
			displaynameHash,
			keyDisplayname: keyDisplayname.data,
			keyDisplaynameIv: keyDisplayname.iv,
		};
	}

	if (avatarStorageId) {
		update.avatarStorageId = avatarStorageId;
	}

	if (Object.keys(update).length > 0) {
		const lastUpdated = new Date();
		update.lastUpdated = lastUpdated;
		await ProfileDatabase.updateOne({ userId, _id: profileId }, update);

		service.getSMQClient()?.publish('TOPIC', `DIRECT.USER.${userId}`, 'PROFILE.UPDATED', { id: profileId, lastUpdated });

		try {
			await axios.put(
				`http://${STACK_NAME}_group-service:3000/members/profile/${profileId}/${lastUpdated.getTime()}`,
				{},
				{
					headers: {
						authorization: SERVICE_TOKEN,
					},
					timeout: TIMEOUT,
				}
			);
		} catch (e) {
			console.log(e);
			throw 'error.server.error';
		}
	}

	res.status(200).send();
});

/**
 * Delete a profile
 * @group Protected
 * @security JWT
 * @route POST /{profileId}
 * @consumes application/json
 * @produces application/json
 * @param {string} displaynameHash.body.required - The hash of the displayname
 * @param {AESEncrypted} displayname.body.required - The displayname encrypted with the masterEncryptionKey
 * @returns {Array} 200 - Array of ProfileEncrypted
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.deleteAsUser('/:profileId', async (req, res, userId) => {
	const { profileId } = req.params;
	verifyParameter(profileId);

	if (!(await ProfileDatabase.findOne({ userId, _id: profileId, active: true }))) {
		throw 'error.profile.notfound';
	}

	if ((await ProfileDatabase.countDocuments({ userId, active: true })) === 1) {
		throw 'error.profile.last';
	}

	try {
		const response = await axios.get<boolean>(`http://${STACK_NAME}_group-service:3000/members/profile/${profileId}`, {
			headers: {
				authorization: SERVICE_TOKEN,
			},
			timeout: TIMEOUT,
		});

		if (response && response.status === 200 && response.data === true) {
			throw 'error.profile.inuse';
		}
	} catch (e) {
		throw 'error.server.error';
	}

	await ProfileDatabase.updateMany(
		{ userId, _id: profileId },
		{ active: false, keyMasterEncryptionKey: undefined, keyMasterEncryptionKeyIv: undefined, avatarStorageId: undefined }
	);

	service.getSMQClient()?.publish('TOPIC', `DIRECT.USER.${userId}`, 'PROFILE.DELETED', { id: profileId });

	res.status(200).send();
});

export default router.getExpressRouter();
